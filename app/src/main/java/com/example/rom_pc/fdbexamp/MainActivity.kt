package com.example.rom_pc.fdbexamp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import java.util.*

class MainActivity : AppCompatActivity() {

    private val text : TextView by lazy {
        findViewById(R.id.text_view) as TextView
    }

    private val button : Button by lazy {
        findViewById(R.id.button) as Button
    }

    private val buttonRemove : TextView by lazy {
        findViewById(R.id.button2) as Button
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener { onClick() }
        buttonRemove.setOnClickListener { onClickRemove() }

        Fdb.showElement(
                {list -> initList(list)},
                {e -> text.text = e})
    }

    private fun onClickRemove() {
        Fdb.removeList { text.text = "" }
    }

    private fun initList(list: MutableList<MyObject>) {
        text.text = ""
        list.forEach { element -> initTextView((element)) }
    }

    fun initTextView(my: MyObject?) {
        text.text = text.text.toString() +
                "\n${my?.name} -- ${my?.number} -- ${my?.data.toString()}"
    }

    fun onClick() {
        val obj = MyObject("NAME ${Random().nextInt(9)}",
                Random().nextInt(50),
                Date(System.currentTimeMillis()))
        Fdb.addElement(obj, {e -> text.text = e})
    }
}
