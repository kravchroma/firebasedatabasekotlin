package com.example.rom_pc.fdbexamp

import java.util.*

/**
 * Created by ROM_PC on 27.08.2016.
 */
class MyObject(var name : String? = null,
               val number : Int? = null,
               val data : Date? = null) {
}