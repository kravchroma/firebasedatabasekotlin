package com.example.rom_pc.fdbexamp

import com.google.firebase.database.*
import java.util.*

/**
 * Created by ROM_PC on 27.08.2016.
 */
object Fdb {

    private val fireBase : FirebaseDatabase by lazy {
        val db = FirebaseDatabase.getInstance()
        db.setPersistenceEnabled(true)
        db
    }

    fun removeList(function: () -> Unit) {
        val reference = fireBase.getReference("list")
        reference.removeValue({ databaseError, databaseReference -> function() })
    }

    fun addElement(myObject: MyObject, callError: (String) -> Unit) {
        val reference = fireBase.getReference("list")
        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                callError(p0.toString())
            }

            override fun onDataChange(p0: DataSnapshot?) {
                reference.push().setValue(myObject)
            }
        })
    }

    fun showElement(call: (MutableList<MyObject>) -> Unit, callError: (String) -> Unit) {
        val reference = fireBase.getReference("list")
        reference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                callError(p0.toString())
            }

            override fun onDataChange(p0: DataSnapshot?) {
                val list = mutableListOf<MyObject>()
                p0?.children?.forEach {
                   element -> list.add(element.getValue(MyObject::class.java))
                }
                call(list)
            }
        })
    }

    //***************************************************************

    fun test() {
        val databaseReference = fireBase.getReference("msg")
        databaseReference.setValue("Hellow World")

        val databaseReference1 = fireBase.getReference("myObj")
        val mo = MyObject("roma", 25, Date(System.currentTimeMillis()))
        databaseReference1.setValue(mo)
    }

    fun getTestData(action1 : (MyObject?) -> Unit, actionError : (String) -> Unit) {
        fireBase.getReference("myObj").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                //Observable.just(p0.toString()).subscribe(actionError)
                actionError(p0.toString())
            }

            override fun onDataChange(p0: DataSnapshot?) {
                val my = p0?.getValue(MyObject::class.java)
                //Observable.just(my).subscribe(action1)
                action1(my)
            }
        })
    }
}